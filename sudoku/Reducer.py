from ValidatePuzzle import ValidatePuzzle
from ReturnPossible import ReturnPossible
from PrintPuzzle import *

class Reducer:
	def __init__(self):
		self.Puzzle = [0]*81
	
	def return_puzzle(self):
		self.poss_values = ReturnPossible()
		self.val_puz = ValidatePuzzle()
		self.printer = PrintPuzzle()
		#asdf
		while True:
			try:
				self.Puzzle.index(0)
		
			except ValueError:
				#my_tuple = tuple(self.Puzzle)
				#return (hash(my_tuple))
				return self.Puzzle
			
			else:
				low = 10
				determined_index = 0
				for each in range(0,81):
					if self.Puzzle[each] == 0:
						size = len(self.poss_values.calculate_possible_values(each,self.Puzzle))
						if low > size:
							low = size
							determined_index = each
				candidates = self.poss_values.calculate_possible_values(determined_index,self.Puzzle)
				if len(candidates) == 0: #dang, this method runs into "dead end" scenarios.
					for each in range((determined_index / 9) * 9, (determined_index / 9) * 9 + 9):
						self.Puzzle[each] = 0
				else:
					self.Puzzle[determined_index] = candidates[0]
					
	def return_puzzle_2(self):
		self.poss_values = ReturnPossible()
		self.val_puz = ValidatePuzzle(self.Puzzle)
		self.printer = PrintPuzzle()
		
		working_list = [
		0,1,2, 9,10,11, 18,19,20,
		3,4,5, 12,13,14, 21,22,23,
		6,7,8, 15,16,17, 24,25,26,
		27,28,29, 36,37,38, 45,46,47,
		30,31,32, 39,40,41, 48,49,50,
		33,34,35, 42,43,44, 51,52,53,
		54,55,56, 63,64,65, 72,73,74,
		57,58,59, 66,67,68, 75,76,77,
		60,61,62, 69,70,71, 78,79,80]
		
		
		while True:
			try:
				self.Puzzle.index(0)
				my_list = []
				for each in working_list:
					if self.Puzzle[each] == 0:
						cube = (each / 9)
						print "Each is: " + str(each)
						break
				for each in range(cube, cube + 9):
					my_list.append(working_list[each])
				self.printer.print_puzzle(self.Puzzle)
				raw_input(my_list)
			
			except ValueError:
				print "Value Error"
				self.printer.print_puzzle(self.Puzzle)
				#self.val_puz.validate_the_Puzzle()
				#my_tuple = tuple(self.Puzzle)
				#return (hash(my_tuple))
				break
			
			else:
				low = 10
				determined_index = 0
				for each in my_list:
					if self.Puzzle[each] == 0:
						size = len(self.poss_values.calculate_possible_values(each,self.Puzzle))
						if low > size:
							low = size
							determined_index = each
				print "Det Index: " + str(determined_index)
				candidates = self.poss_values.calculate_possible_values(determined_index,self.Puzzle)
				if len(candidates) == 0: #dang, this method runs into "dead end" scenarios.
					print "Had to back track"
					for each in range((determined_index / 9) * 9, (determined_index / 9) * 9 + 9):
						self.Puzzle[each] = 0
				else:
					self.Puzzle[determined_index] = candidates[0]