'''
Created on Sep 6, 2012

@author: dennis_vostro_laptop
'''
import unittest
from PrintPuzzle import *

class Test(unittest.TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def testOne(self):
        print_puzzle = PrintPuzzle()
        self.failUnless(print_puzzle.is_odd(1))
        
if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()