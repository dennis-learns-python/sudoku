'''
Created on Sep 8, 2012

@author: dennis_vostro_laptop
'''

import sqlite3

class DB:
    '''May as well save the created puzzles'''
    
    def __init__(self, location=None):
        '''Constructor'''
        self.connect()
    
    def __del__(self):
        self.cursor.close()
        '''DB Desctructor Ran'''
     
    def connect(self, location="puzzles.sqlite3.db"):
        self.connection = sqlite3.connect(location)
        self.cursor = self.connection.cursor()
        self.cursor.execute('CREATE TABLE IF NOT EXISTS PUZZLES (Hash INT, puz VARCHAR(81) )')
    
    def save_puzzle(self, puz):
        puz_hash = hash(tuple(puz))
        print puz_hash
        puz = str(puz)
        puz = puz.translate(None, '\'[],')
        puz = puz.replace(' ', '')
        
        self.cursor.execute('INSERT INTO PUZZLES VALUES (?, ?)', (puz_hash, puz,)) #Must have ',' to make a tuple!
        self.connection.commit()
    
    def retrieve_puzzle(self):
        self.cursor.execute('SELECT * FROM PUZZLES')
        my_tuple = self.cursor.fetchall()
        for each in my_tuple:
            print each
    
    def work(self):
        pass
    
    def count(self):
        '''How many puzzles'''
        #return self.cursor.rowcount
        print self.cursor.execute('SELECT * FROM PUZZLES')
        return self.cursor.lastrowid