import random
class ReturnPossible:
	def __init__(self):
		pass
		
	def calculate_possible_values(self, index, Puzzle):
		self.index = index
		self.Puzzle = Puzzle
		#print "Index " + str(self.index)
		
		#Column brothers
		column = self.index % 9
		column_contents = set(self.Puzzle[column:81:9])
		#print "Column " + str(column) + " Contains: " + str(column_contents)
		
		#Row brothers
		row = self.index / 9
		row_contents = set(self.Puzzle[row*9:(row*9)+9])
		#print "Row " +str(row) + " Contains: " + str(row_contents)

		#Cube brothers
		cube_column = column / 3
		cube_row = row / 3
		#Cube Corner
		cube_corner = cube_row*27 + cube_column*3
		#print "Cube Column: " + str(cube_column) + " Cube Row: " + str(cube_row) + " Cube Corner: " + str(cube_corner)
		#Cube contains
		cube_indexes = range(cube_corner,cube_corner+3) + range(cube_corner+9,cube_corner+12) + range(cube_corner+18,cube_corner+21)
		#print "Cube idexes: " + str(cube_indexes)
		cube_contents = set()
		for each in cube_indexes:
			cube_contents.add(self.Puzzle[each])
		#print "Cube Contains: " + str(cube_contents)
		
		#Return what could work at the index
		cube_contents.update(column_contents)
		cube_contents.update(row_contents)
		#print "All unavailble for index " + str(self.index) + ": " + str(cube_contents)
		my_list = list(set(range(1,10)) - set(cube_contents))
		random.shuffle(my_list)
		#print my_list
		return my_list