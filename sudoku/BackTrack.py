from ReturnPossible import *

class BackTrack:
	def __init__(self):
		self.Puzzle_index = 0
		self.Sticky_index = 0
		self.Stuck_strength = 0
		self.Puzzle = [0]*81
		self.poss_values = ReturnPossible()
	
	def return_puzzle(self):
		while self.Puzzle_index < 81:
			candidates = self.poss_values.calculate_possible_values(self.Puzzle_index,self.Puzzle)
			
			try:
				self.Puzzle[self.Puzzle_index] = candidates[0]
				
			except LookupError:
				if self.Sticky_index < self.Puzzle_index:
					self.Sticky_index = self.Puzzle_index
				if self.Puzzle_index == self.Sticky_index:
					self.Stuck_strength+=1 #Go further back everytime the same index is blocked
				for each in range(self.Puzzle_index - self.Stuck_strength, self.Puzzle_index+1):
					self.Puzzle[each] = 0
				self.Puzzle_index -= self.Stuck_strength
				
			else:
				if self.Puzzle_index == self.Sticky_index:
					self.Sticky_index = 0
					self.Stuck_strength = 0
				self.Puzzle_index += 1

		return self.Puzzle