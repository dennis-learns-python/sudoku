class ValidatePuzzle:

	def __init__(self):
		pass
	
	def validate_the_Puzzle(self, puzzle=[0]*81):
		self.puzzle=puzzle
		
		print '''\nValidating puzzle for Columns, Rows, or Cubes that don't: \nsum to 45 \nhave the digits 1 - 9'''
		
		print "\tValidating Rows:" + self.validate_rows(self.puzzle)
		
		print "\tValidating Columns:" + self.validate_columns(self.puzzle)
		
		print "\tValidating Cubes:" + self.validate_cubes(self.puzzle)
		
		print "Validation complete."
	
	def validate_rows(self, puzzle=[0]*81):
		self.puzzle=puzzle
		self.message = " Done."
		
		for each in range(0,81,9):
			row_set = set()
			for row in range(each, each+9):
				row_set.add(self.puzzle[row])
			if len(row_set) != 9 or sum(row_set) != 45:
				self.message += "\n\t\tRow " + str(each/9 + 1) + " has " + str(len(row_set)) + " unique numbers that sum to " + str(sum(row_set))
			return self.message
		
	def validate_columns(self, puzzle=[0]*81):
		self.puzzle=puzzle
		self.message = " Done."
		
		for each in range(0,9):
			column_set = set()
			for column in range(each, 81, 9):
				column_set.add(self.puzzle[column])
			if len(column_set) != 9 or sum(column_set) != 45:
				self.message += "\n\t\tColumn " + str(each + 1) + " has " + str(len(column_set)) + " unique numbers that sum to " + str(sum(column_set))
			return self.message
		
	def validate_cubes(self, puzzle=[0]*81):
		self.puzzle=puzzle
		self.message = " Done."
		
		for each in (0,3,6, 27,30,33, 54,57,60):
			cube_set = set()
			for row in range(each,each+19,9):
				for item in range(row,row+3):
					cube_set.add(self.puzzle[item])	
			if len(cube_set) != 9 or sum(cube_set) != 45:
				self.message += "\n\t\tCube with base at " + str(each) + " has " + str(len(cube_set)) + " unique numbers that sum to " + str(sum(cube_set))
			return self.message