#!/usr/bin/python

import sys
import time

from BackTrack import *
from Reducer import *

from ValidatePuzzle import ValidatePuzzle
from PrintPuzzle import *

from DB import *

def main():
	validate_puzzle = ValidatePuzzle()
	printer = PrintPuzzle()
	my_db = DB()	
	
	
	reducer = Reducer()
	puzzle = reducer.return_puzzle()
	printer.print_puzzle(puzzle)
	validate_puzzle.validate_the_Puzzle(puzzle)
	my_db.save_puzzle(puzzle)
		
	backtrack = BackTrack()
	puzzle = backtrack.return_puzzle()
	printer.print_puzzle(puzzle)
	validate_puzzle.validate_the_Puzzle(puzzle)
	my_db.save_puzzle(puzzle)
	
	#my_db.retrieve_puzzle()
	print "There are " + str(my_db.count()) + " puzzles."
	
	#print sys.argv[1:3]
	#timez = ""
	#t0 = time.clock()
	#t1 = time.clock()
	#timez += str((t1 - t0) * 1000) + " seconds process time\n"
	
if __name__ == '__main__':
	main()